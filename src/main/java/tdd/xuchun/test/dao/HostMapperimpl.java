package tdd.xuchun.test.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.db.Db;
import cn.hutool.db.Entity;
import tdd.xuchun.test.model.Host;

/**
 * 涓绘満DAO鏁版嵁鎺ュ彛
 * 
 * @author ZSL
 *
 */
@Repository
public class HostMapperimpl implements HostMapper {

	/**
	 * 鏌ヨ鍗曚釜涓绘満鏄庣粏
	 * 
	 * @param host
	 * @return
	 */
	public Host getHost(Host host) {
		List<Entity> list;
		try {
			list = Db.use().query("select * from t_host where id = ?", host.getId());

			if (list.size() <= 0) {
				host = new Host();
			} else {
				Entity hosten = list.get(0);
				host = entity2Host(hosten);

			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return host;
	};

	/**
	 * 鏌ヨ鍗曚釜涓绘満鏄庣粏鍒╃敤IP
	 * 
	 * @param host
	 * @return
	 */
	public Host getHostByIP(Host host) {
		List<Entity> list;
		try {
			list = Db.use().query("select * from t_host where addr = ?", host.getAddr());

			if (list.size() <= 0) {
				host = null;
			} else {
				Entity hosten = list.get(0);
				host = entity2Host(hosten);

			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return host;
	};

	/**
	 * 澧炲姞鏂颁富鏈�
	 * 
	 * @param host
	 */
	public void addHost(Host host) {
		try {
			Db.use().execute(
					"insert into t_host(name,addr,port,username,password,remark,eznmonport,ostype,keyfilepath)"
							+ " values(?,?,?,?,?,?,?,?,?)",
					host.getName(), host.getAddr(), host.getPort(), host.getUsername(), host.getPassword(),
					host.getRemark(), host.getEznmonport(), host.getOstype(),host.getKeyfilepath());
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	};

	/**
	 * 鍒犻櫎涓绘満
	 * 
	 * @param host
	 */
	public void delHost(Host host) {
		try {
			Db.use().execute("delete from t_host where id = ?", host.getId());
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	};

	/**
	 * 淇敼涓绘満
	 * 
	 * @param host
	 */
	public void editHost(Host host) {
		try {
			Db.use().execute(
					"update t_host set name=?,addr = ?,username =? ,port = ? , "
							+ "password=? , remark=? ,eznmonport=?,ostype=?,keyfilepath=?  where id = ?",
					host.getName(), host.getAddr(), host.getUsername(), host.getPort(), host.getPassword(),
					host.getRemark(), host.getEznmonport(), host.getOstype(), host.getKeyfilepath(),host.getId());
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	};

	/**
	 * 妯＄硦鏌ヨ鍖归厤鐨勪富鏈哄垪琛�
	 * 
	 * @param host
	 * @return
	 */
	public List<Object> getLikeHosts(Map<String, Object> hosts) {
		Host host = (Host) hosts.get("host");
		int pageSize = Integer.parseInt(String.valueOf(hosts.get("pageSize")));
		int curPageNum = Integer.parseInt(String.valueOf(hosts.get("pageNum")));
		List list = new ArrayList();
		List rlist = new ArrayList();

		try {
			list = Db.use()
					.query("select * from t_host where name like '%" + host.getName() + "%' and  addr like '%"
							+ host.getAddr() + "%' and remark like '%" + host.getRemark() + "%' limit " + pageSize
							+ "*(" + curPageNum + "-1)," + pageSize + " ");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		for (int i = 0; i < list.size(); i++) {
			Host ihost = entity2Host((Entity) list.get(i));
			HashMap map = (HashMap) BeanUtil.beanToMap(ihost);
			rlist.add(map);
		}

		return rlist;
	};

	/**
	 * 妯＄硦鏌ヨ鍖归厤鐨勪富鏈虹殑鏁伴噺
	 * 
	 * @param host
	 * @return
	 */
	public Integer getLikeHostsCount(Map<String, Object> hosts) {
		Host host = (Host) hosts.get("host");
		List<Entity> list = new ArrayList();

		try {
			list = Db.use().query("select * from t_host where name like '%" + host.getName() + "%' and  addr like '%"
					+ host.getAddr() + "%' and remark like '%" + host.getRemark() + "%'");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return Integer.valueOf(list.size());

	};

	private Host entity2Host(Entity hosten) {
		Host host = new Host();
		host.setId(hosten.getInt("id"));
		host.setAddr(hosten.getStr("addr"));
		host.setEznmonport(hosten.getInt("eznmonport"));
		host.setName(hosten.getStr("name"));
		host.setPassword(hosten.getStr("password"));
		host.setPort(hosten.getInt("port"));
		host.setRemark(hosten.getStr("remark"));
		host.setUsername(hosten.getStr("username"));
		host.setOstype(hosten.getStr("ostype"));
		host.setKeyfilepath(hosten.getStr("keyfilepath"));
		return host;
	}

}
