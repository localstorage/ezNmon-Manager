package tdd.xuchun.test.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.springframework.core.io.ClassPathResource;

public class TarUntil {
	/*
	 * 返回包中的文件名
	 */
	public static List<String> visitTARGZ(File targzFile) throws IOException {
		List<String> filenamelist = new ArrayList<String>();
		FileInputStream fileIn = null;
		BufferedInputStream bufIn = null;
		GZIPInputStream gzipIn = null;
		TarArchiveInputStream taris = null;
		try {
			fileIn = new FileInputStream(targzFile);
			bufIn = new BufferedInputStream(fileIn);
			gzipIn = new GZIPInputStream(bufIn);
			taris = new TarArchiveInputStream(gzipIn);
			TarArchiveEntry entry = null;
			while ((entry = taris.getNextTarEntry()) != null) {
				if (entry.isDirectory()) {
					continue;
				}
				filenamelist.add(entry.getName());
			}
		} finally {
			taris.close();
			gzipIn.close();
			bufIn.close();
			fileIn.close();
		}
		return filenamelist;
	}
	//将jar包中的文件存到硬盘上并返回硬盘上的文件
	public static  File getdiskFile(String  jarpath) throws IOException {
		FileOutputStream fileout=null;
		ClassPathResource cpr = new ClassPathResource(jarpath);
		File fout = new File(cpr.getFilename());
		if(fout.exists()){
			return fout;
		}
		InputStream is =cpr.getInputStream();
		FileOutputStream fos = new FileOutputStream(cpr.getFilename());
		byte[] b = new byte[1024];
		int length;
		while((length=is.read(b))>0){
			fos.write(b,0,length);
		}
		is.close();
		fos.close();
		fout = new File(cpr.getFilename());
		return fout;
	}
}
